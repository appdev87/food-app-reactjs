const mongoose = require('mongoose');
const { Schema, model } = mongoose;

const ItemsSchema = new Schema({
  id: Number,
  name: String,
  amount: Number,
  price: Number
});

const CheckoutSchema = new Schema({
  id: String,
  name: String,
  street: String,
  postalCode: String,
  city: String,
  orderItems: [ItemsSchema],
  totalAmount: Number
});

const CheckoutModel = model('checkout', CheckoutSchema);
module.exports = CheckoutModel;