const mongoose = require('mongoose');
const { Schema, model } = mongoose;

const MealSchema = new Schema({
  id: String,
  name: String,
  price: Number,
  description: String,
});

const MealModel = model('meals', MealSchema);
module.exports = MealModel;