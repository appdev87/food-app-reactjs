const express = require('express');
const app = express();
const cors = require('cors');
const mongoose = require('mongoose');

const MealModel = require('./models/MealModel');
const CheckoutModel = require('./models/CheckoutModel');

require('dotenv').config();
const env = process.env;

app.use(cors());
app.use(express.json());

mongoose.connect(env.FOODAPP_DB_URI);

app.get('/getMeals', (req, res) => {

  MealModel.find({}, (err, data) => {
    if (err) {
      res.json(err);
    } else {
      res.json(data);
    }
  });

})

app.get('/getOrders', (req, res) => {

  CheckoutModel.find({}, (err, data) => {
    if (err) {
      res.json(err);
    } else {
      res.json(data);
    }
  });

})


app.get('/getOrder?name=:name', (req, res) => {
  console.log(req.params.name);
  const name = req.params.name;

  
  CheckoutModel.find({ name: name }, (err, data) => {
    if (err) {
      res.json(err);
    } else {
      res.json(data);
    }
  });

})

app.post('/checkoutOrder', async (req, res) => {

  const checkout = req.body;
  const checkoutMeal = new CheckoutModel(checkout);
  await checkoutMeal.save();

  res.json(checkout);

})


app.listen(env.PORT, () => {
  console.log(`Listening on port: ${env.PORT}`);
})