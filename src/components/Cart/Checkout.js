import { useContext, useRef, useState } from 'react';
import styles from './Checkout.module.css';

import CartContext from '../../store/cart-context';

const isEmpty = (value) => value.trim() === '';


const Checkout = (props) => {
  const cartCtxCheckout = useContext(CartContext);
  const { items, totalAmount } = cartCtxCheckout;

  const nameRef = useRef();
  const streetRef = useRef();
  const postalCodeRef = useRef();
  const cityRef = useRef();

  const [ isFormValid, setFormValidityState ] = useState({
    name: true,
    street: true,
    postalCode: true,
    city: true,
  });

  const confirmHandler = (event) => {
    event.preventDefault();

    const amountDue = totalAmount.toFixed(2);

    const newOrder = {
      name: nameRef.current.value,
      street: streetRef.current.value,
      postalCode: postalCodeRef.current.value,
      city: cityRef.current.value,
      orderItems: items,
      totalAmount: amountDue,
    };

    const nameInputIsValid = !isEmpty(newOrder.name);
    const streetInputIsValid = !isEmpty(newOrder.street);
    const postalCodeInputIsValid = !isEmpty(newOrder.postalCode);
    const cityInputIsValid = !isEmpty(newOrder.city);

    const formIsValid = nameInputIsValid && cityInputIsValid && streetInputIsValid && postalCodeInputIsValid;

    if (!formIsValid) {
      return;
    }

    props.onConfirm(newOrder);

      setFormValidityState({
        name: nameInputIsValid,
        city: cityInputIsValid,
        street: streetInputIsValid,
        postalCode: postalCodeInputIsValid
      });

      nameRef.current.value = '';
      streetRef.current.value = '';
      postalCodeRef.current.value = '';
      cityRef.current.value = '';
    
  };

  return (
    <form onSubmit={confirmHandler}>
      <div className={`${styles.control} ${isFormValid.name ? '' : styles.invalid}`}>
        <label htmlFor='name'>Your name</label>
        <input type='text' id='name' ref={nameRef} />
        {!isFormValid.name && <p style={{ color: 'red' }}>Please enter a valid name</p>}
      </div>

      <div className={`${styles.control} ${isFormValid.street ? '' : styles.invalid}`}>
        <label htmlFor='street'>Street</label>
        <input type='text' id='street' ref={streetRef} />
        {!isFormValid.street && <p style={{ color: 'red' }}>Please enter a valid Street</p>}
      </div>

      <div className={`${styles.control} ${isFormValid.postalCode ? '' : styles.invalid}`}>
        <label htmlFor='postal'>Postal Code</label>
        <input type='text' id='postal' ref={postalCodeRef} />
        {!isFormValid.postalCode && <p style={{ color: 'red' }}>Please enter a valid Postal Code</p>}
      </div>

      <div className={`${styles.control} ${isFormValid.city ? '' : styles.invalid}`}>
        <label htmlFor='city'>City</label>
        <input type='text' id='city' ref={cityRef} />
        {!isFormValid.city && <p style={{ color: 'red' }}>Please enter a valid city</p>}
      </div>

      <div className={`${styles.actions}`}>
        <button className={styles.button} type='button' onClick={props.onCancel}>
          Cancel
        </button>
        <button className={styles.button} type='submit'>
          Confirm
        </button>
      </div>

    </form>
  );
};

export default Checkout;
