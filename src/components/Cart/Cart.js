import React, { useContext, useState } from 'react';
import { BsFillBagCheckFill } from 'react-icons/bs';
import axios from 'axios';

import Modal from '../UI/Modal';
import CartItem from './CartItem';
import classes from './Cart.module.css';
import CartContext from '../../store/cart-context';
import Checkout from './Checkout';


const Cart = (props) => {
  const [ isCheckout, setIsCheckout ] = useState(false);
  const [ isSubmitting, setIsSubmitting ] = useState(false);
  const [ didSubmit, setDidSubmit ] = useState(false);

  const cartCtx = useContext(CartContext);

  const totalAmount = `$${cartCtx.totalAmount.toFixed(2)}`;
  const hasItems = cartCtx.items.length > 0;

  const cartItemRemoveHandler = (id) => {
    cartCtx.removeItem(id);
  };

  const cartItemAddHandler = (item) => {
    cartCtx.addItem(item);
  };

  const cartItems = (
    <ul className={classes[ 'cart-items' ]}>
      {cartCtx.items.map((item) => (
        <CartItem
          key={item.id}
          name={item.name}
          amount={item.amount}
          price={item.price}
          onRemove={cartItemRemoveHandler.bind(null, item.id)}
          onAdd={cartItemAddHandler.bind(null, item)}
        />
      ))}
    </ul>
  );

  const orderHandler = () => {
    setIsCheckout(true);
  };

  const submitOrderHandler = async (newOrder) => {
    setIsSubmitting(true);

    await axios
      .post('http://localhost:3001/checkoutOrder', newOrder)
      .then((response) => {
        console.log(newOrder);
      })
      .catch((error) => {
        alert(`Error posting order: ${error.message}`);
      });

    setIsSubmitting(false);
    setDidSubmit(true);
    cartCtx.clearCart();
  }

  const modalActions = (
    <div className={classes.actions}>
      <button className={classes[ 'button--alt' ]} onClick={props.onClose}>
        Close
      </button>
      {hasItems && (
        <button className={classes.button}
          onClick={orderHandler}>
          Order
        </button>
      )}
    </div>
  );

  const cartModalContent = <React.Fragment>
    {cartItems}
    <div className={classes.total}>
      <span>Total Amount</span>
      <span>{totalAmount}</span>
    </div>
    {isCheckout && <Checkout onCancel={props.onClose} onConfirm={submitOrderHandler} />}
    {!isCheckout && modalActions}
  </React.Fragment>;

  const isSubmittingModalContent = <p>Sending order data...</p>
  const didSubmitModalContent = <React.Fragment><h1><BsFillBagCheckFill />Order submitted</h1></React.Fragment>

  return (
    <Modal onClose={props.onClose}>
      {!isSubmitting && !didSubmit && cartModalContent}
      {isSubmitting && isSubmittingModalContent}
      {!isSubmitting && didSubmit && didSubmitModalContent}
    </Modal>
  );
};

export default Cart;
