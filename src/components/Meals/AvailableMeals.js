import Card from '../UI/Card';
import MealItem from './MealItem/MealItem';
import classes from './AvailableMeals.module.css';

import { useState, useEffect } from 'react';
import axios from 'axios';

const AvailableMeals = () => {
  const [meals, setMealsList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [errorMessage, setErrorMessage] = useState('');

  useEffect(() => {
    axios
      .get('http://localhost:3001/getMeals')
      .then((response) => {
        if (response) {
          setMealsList(response.data);
          setIsLoading(false);
        }
      })
      .catch((error) => {
        setErrorMessage(`The following error occurred: ${error.response.status}`);
      });
  }, []);

  const mealsList = meals.map((meal, id) => <MealItem key={meal._id} id={id} name={meal.name} description={meal.description} price={meal.price} />);
  const dataIsLoaded = !isLoading ? <ul>{mealsList}</ul> : 'Loading data...';
  const dataSuccessfullyLoaded = errorMessage ? <h1>{errorMessage}</h1> : dataIsLoaded;

  return (
    <section className={classes.meals}>
      <Card>{dataSuccessfullyLoaded}</Card>
    </section>
  );
};

export default AvailableMeals;
